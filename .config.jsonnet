local ci = import '.ci.jsonnet';

// GitLab CI
{
  '.gitlab-ci.json': {
                       // Default to branch pipelines because code is mirrored on ops where there are not
                       // any MRs
                       include: [
                         { template: 'Workflows/Branch-Pipelines.gitlab-ci.yml' },
                       ],

                       default: {
                         image: 'registry.gitlab.com/gitlab-com/gl-infra/ci-images/gitlab-config-management:1b499df3',
                       },

                       stages: [
                         'Prepare',
                         'Check Remote',
                         'Verify',
                         'Test',
                         'Deploy',
                       ],
                     }
                     + ci.notifyMirrorSourceJob()
                     + ci.checkRemoteJob()
                     + ci.lintCIJob()
                     + ci.lintAnsibleJob()
                     + ci.environmentJobs()
} + {
  ["ci/%s.json" % env]: {
                          stages: [
                                    "Deploy",
                                    "Verify",
                                  ],
                        }
                        + ci.ansibleJobs(env)
  for env in std.objectFields(ci.environments)
} + {

  // Ansible configuration GCP

  ['ansible/environments/%s/inventory.gcp.json' % env]: {
    plugin: 'gcp_compute',
    projects: [ci.environments[env].gcp_project],
    filters: ["name ne 'gke-.*'"],
    keyed_groups: [
      { key: 'labels.pet_name', separator: '' },
    ],
    scopes: ['https://www.googleapis.com/auth/compute'],
    // List host by name instead of the default public ip
    hostnames: ['name'],
    // Set an inventory parameter to use the Public IP address to connect to the host
    // For Public ip use "networkInterfaces[0].accessConfigs[0].natIP"
    compose: [{ ansible_host: 'networkInterfaces[0].networkIP' }],
    auth_kind: 'serviceaccount',
    regions: ['us-east1'],
  }
  for env in std.objectFields(ci.environments)
}

+

// Ansible configuration vars
{
  ['ansible/environments/%s/vars.json' % env]: {
    all: {
      vars: {
        ansible_user: ci.environments[env].ansible_user,
        ansible_ssh_private_key_file: "{{ lookup('env', 'ANSIBLE_SSH_PRIVATE_KEY_FILE') }}",
        cloud_provider: 'gcp',
        gcp_project: ci.environments[env].gcp_project,

        // Bastion configuration
        bastion_ip: "{{ (groups['bastion'] | sort | map('extract', hostvars, external_ip_lookup[cloud_provider]) | join('')) if 'bastion' in groups else '' }}",
        // StrictHostKeyChecking=no needed for CI. See https://gitlab.com/gitlab-com/gl-infra/ansible-workloads/db-provisioning/-/merge_requests/22/#note_638896128
        bastion_proxy_command: '-o ProxyCommand="ssh -o StrictHostKeyChecking=no -W %h:%p -i {{ ansible_ssh_private_key_file }} {{ ansible_user }}@{{ bastion_ip }}"',
        ansible_ssh_common_args: "{{ bastion_proxy_command if bastion_ip != '' else '' }}",
      },
    },
  }
  for env in std.objectFields(ci.environments)
}
