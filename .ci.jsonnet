local woodhouseImg = 'registry.gitlab.com/gitlab-com/gl-infra/woodhouse:latest';

local envs = {
  environments: {
    gprd: {
      ansible_user: 'sa_109205449689272861124',
      gcp_project: 'gitlab-production',
    },
    gstg: {
      ansible_user: 'sa_102183625600071103060',
      gcp_project: 'gitlab-staging-1',
    },
    pre: {
      ansible_user: 'sa_117808528763118509348',
      gcp_project: 'gitlab-pre',
    },
  },
};

local basePlays = [
  "common",
];


// Conditions

local cond = {
  ifNotTag: { 'if': '$CI_COMMIT_TAG', when: 'never' },
  ifNotNonDefaultBranch: { 'if': '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH', when: 'never' },
  ifNotDefaultBranch: { 'if': '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH', when: 'never' },
  ifMirror: { 'if': '$MIRROR == "true"' },
  ifMirrorManual: { 'if': '$MIRROR == "true"', when: 'manual' },
  ifCanonical: { 'if': '$CANONICAL == "true"' },
};

// Rules

local rules = {
  Canonical: { rules: [cond.ifNotTag, cond.ifCanonical] },
  Mirror: { rules: [cond.ifNotTag, cond.ifMirror] },
  MirrorManual: { rules: [cond.ifNotTag, cond.ifMirrorManual] },
  MirrorDefaultBranch: { rules: [cond.ifNotTag, cond.ifNotNonDefaultBranch, cond.ifMirror] },
  MirrorDefaultBranchManual: { rules: [cond.ifNotTag, cond.ifNotNonDefaultBranch, cond.ifMirror] },
};

local envAnsible(env, play) = {
 local inventory = 'environments/%s/' % env,
 local playFilename = '%s.yml' % play,
 script: [
   'cd $CI_PROJECT_DIR/ansible',
   'echo Creating symlinks for GET',
   'ln -s /opt/get/ansible/group_vars',
   'ln -s /opt/get/ansible/roles get_roles',
   // Necessary for Ansible inventory
   // Note that this cannot be set in a `variables:` block as it will set
   // the variable to the file's contents, instead of the file path.
   'export GCP_SERVICE_ACCOUNT_FILE=$GOOGLE_APPLICATION_CREDENTIALS',
   // To avoid the "Ansible is being run in a world writable" directory error
   'export ANSIBLE_CONFIG=ansible.cfg',
   'echo "Running: ansible-playbook -i %s %s"' % [inventory, playFilename],
   'ansible-playbook -i %s %s' % [inventory, playFilename],
 ],
 allow_failure: true,
 environment: {
   name: env,
 },
};

// Jobs for the primary pipeline
// --------------------------------
// These functions are for the main pipeline, up until
// the environment trigges

{
  notifyMirrorSourceJob():: {
    'notify mirror source': rules.Mirror {
      stage: 'Prepare',
      allow_failure: true,
      image: woodhouseImg,
      script: 'woodhouse gitlab notify-mirrored-mr',
    },
  },

   lintCIJob():: {
     'ci config generated': rules.Mirror {
       stage: 'Prepare',
       script: [
           'apk add make',
            'make generate-config',
            'if ! git diff --exit-code; then echo "Error: jsonnet configuration updated but configuration files have not been re-generated and committed. Run `make generate-config` and commit the updated rendered config files." && exit 1; fi',
       ],
     },
   },

    lintAnsibleJob():: {
        'ansible lint': rules.Mirror {
            stage: 'Verify',
            script: [
                'cd ansible',
                'ansible-lint -v',
            ],
        },
    },

  checkRemoteJob():: {
    'check remote': rules.Canonical {
      stage: 'Check Remote',
      image: woodhouseImg,
      script: 'woodhouse gitlab follow-remote-pipeline --gitlab-api-base-url="https://ops.gitlab.net/api/v4" --gitlab-api-token="$OPS_API_TOKEN"',
    },
  },

  environmentJobs():: {
    ['%s:deploy' % env]: rules.MirrorManual {
      stage: "Deploy",
      allow_failure: true,
      trigger: {
        include: [
          { template: "Workflows/Branch-Pipelines.gitlab-ci.yml" },
          { "local": "/ci/%s.yml" % env },
        ],
      },
    }
    for env in std.objectFields(envs.environments)
  },

  // Jobs for environments
  // --------------------------------
  // These functions are for jobs in the child pipeline

  ansibleJobs(env):: {
    // We may be able to switch this to only display the play eventually, see
    // https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/24
    ["%s:%s" % [play, env]]: rules.MirrorManual + envAnsible(env, play) 
    for play in basePlays
  },

} + envs
